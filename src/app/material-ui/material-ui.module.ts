import { NgModule } from '@angular/core';
import { MatDialogModule, MatButtonModule, MatRippleModule } from '@angular/material';

const MaterialUIComponents = [
  MatDialogModule,
  MatButtonModule,
  MatRippleModule
];

@NgModule({
  imports: [MaterialUIComponents],
  exports: [MaterialUIComponents]
})
export class MaterialUIModule { }
