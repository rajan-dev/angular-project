import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserNavbarComponent } from './user-navbar/user-navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserLoginSignupComponent } from './user-login-signup/user-login-signup.component';
import { MaterialUIModule } from './material-ui/material-ui.module';

@NgModule({
  declarations: [
    AppComponent,
    UserNavbarComponent,
    UserLoginSignupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialUIModule
  ],
  entryComponents: [UserLoginSignupComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
