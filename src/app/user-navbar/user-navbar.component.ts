import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { UserLoginSignupComponent } from '../user-login-signup/user-login-signup.component';

@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.sass']
})
export class UserNavbarComponent implements OnInit {

  @Input() siteTitle: string 
  constructor(public loginPopup: MatDialog) { }

  ngOnInit() {
  }

  openPopup(): void {
    let popRef = this.loginPopup.open(UserLoginSignupComponent,{
      width: '400px'
    })
  }

}
